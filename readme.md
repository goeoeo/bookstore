# 代码过程

## API Gateway服务

1. 生成api文档 #当前目录 api  
goctl api -o bookstore.api 


2. 根据api文档 生成目录以及相关代码  
goctl api go -api bookstore.api -dir . 

3. 启动api 服务  
go run bookstore.go -f etc/bookstore-api.yaml 

4. 测试  
curl -i "http://localhost:8888/check?book=go-zero"



## Rpc Add 服务  #当前目录 rpc/add
1. 生成 add.proto 文件   
goctl rpc template -o add.proto 

2. 根据 add.proto 文件 生成rpc服务相关代码   
goctl rpc proto -src add.proto -dir .

3. 根据表结构生存model  
goctl model mysql ddl -c -src book.sql -dir . 

4. 修改代码  

5. 启动  

go run add.go -f etc/add.yaml
