module bookstore

go 1.13

require (
	github.com/golang/protobuf v1.4.2
	github.com/tal-tech/go-zero v1.1.6
	google.golang.org/grpc v1.29.1
)
